package net.thumbtack.tutorial.spring;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityNotFoundException;
import java.util.stream.Stream;

import static org.junit.Assert.assertNotNull;

/**
 * Test suite for {@link AccountRepository}.
 */
public class AccountRepositoryTest extends AbstractRepositoryTestSuite {
	@Autowired
	private AccountRepository repository;

	@Test
	public void returnsAll() {
		try (Stream<Account> items = repository.findAll()) {
			assertNotNull(items);
		}
	}
}
