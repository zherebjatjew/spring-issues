package net.thumbtack.tutorial.spring;

import org.springframework.data.repository.Repository;

import java.util.stream.Stream;

/**
 * Account dao.
 */
public interface AccountRepository extends Repository<Account, Long> {
	Account findOne(Long id);
	Stream<Account> findAll();
}
