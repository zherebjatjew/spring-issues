package net.thumbtack.tutorial.spring;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

/**
 * Just a stub to have an aspect.
 */
@Aspect
@Component
public class TestAspect {

	@Around("execution(public * *.findOne(Long))")
	public Object wrap(ProceedingJoinPoint pjp) throws Throwable {
		return pjp.proceed();
	}
}
