package net.thumbtack.tutorial.spring;

import org.springframework.data.repository.CrudRepository;

/**
 * Account dao.
 */
public interface AccountRepository extends CrudRepository<Account, Long> {
}
