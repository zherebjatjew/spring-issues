package net.thumbtack.tutorial.spring;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityNotFoundException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * Test suite for {@link AccountRepository}.
 */
public class AccountRepositoryTest extends AbstractRepositoryTestSuite {
	@Autowired
	private AccountRepository repository;

	@Test(expected = EntityNotFoundException.class)
	public void reportsOnUnknownId() {
		repository.findOne(-1L);
	}

	@Test
	public void silentlyDeletesUnknownObjects() {
		repository.delete(-1L);
	}
}
